import Button from "./Button";
import PropTypes from 'prop-types';
import Modal from "./Modal";
import {useState} from "react";
import IconStarCard from "../icon/IconStarCard";

function ProductCard({id, racingCar, toggleCounterSelected, toggleCounterFavorites}) {

    const [showModal, setShowModal] = useState(false);
    const {imageUrl, name, color, price, completed, buyCar} = racingCar;

    let text = "Are you sure you want to add the " + racingCar.name + " to the cart?";

    const toggleModal = () => {
        setShowModal(!showModal);
    };

    let modalActions = (
        <div className="button-container">
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Yes" onClick={() => {
                toggleModal();
                toggleCounterSelected(id)
            }}/>
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="No" onClick={toggleModal}/>
        </div>
    );

    if (buyCar === true) {
        text = "Have you already added " + name + " to your cart";
        modalActions = (
            <div className="button-container">
                <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Close" onClick={toggleModal}/>
            </div>
        )
    }

    return (
        <li className="car-item">
            <div className="img-container">
                <img className={"car-img"} src={imageUrl} alt={name}/>
            </div>
            <h2 className="car-name">{name}</h2>
            <p className="car-info">Цвет: {color}</p>
            <p className="car-info">Цена: ${price.toLocaleString()}</p>
            <Button text="Add to cart" onClick={() => toggleModal()}/>
            <div onClick={() => {toggleCounterFavorites(id)}} className={`car-item-icon ${completed && "completed"}`}>
                <IconStarCard/>
            </div>
            {showModal && (<Modal header="Confirmation" closeButton={true} onClick={toggleModal} text={text} actions={modalActions}/>)}
        </li>
    );
}

ProductCard.propTypes = {
    id: PropTypes.number,
    racingCar: PropTypes.object,
    toggleCounterFavorites: PropTypes.func,
    toggleCounterSelected: PropTypes.func
};

export default ProductCard;
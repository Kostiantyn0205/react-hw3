import ProductCard from "./ProductCard";
import PropTypes from 'prop-types';
import ProductCardBasket from "./ProductCardBasket";

function MainPage({pageName, racingCars, toggleCounterSelected, toggleCounterFavorites}) {
    return (
        <>
            <ul className="car-list">
                {racingCars.map((car) => (
                    pageName === 'Basket' ? <ProductCardBasket key={car.idCar} id={car.idCar} racingCar={car} toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}/> :
                        <ProductCard key={car.idCar} id={car.idCar} racingCar={car} toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}/>
                ))}
            </ul>

            {!racingCars.length && (<h1 className={"not-selected"}>You have not selected a product</h1>)}
        </>
    );
}

MainPage.propTypes = {
    pageName: PropTypes.string,
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default MainPage;
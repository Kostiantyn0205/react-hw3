import Button from "./Button";
import PropTypes from 'prop-types';
import Modal from "./Modal";
import {useState} from "react";
import MainPage from "./ListCard;";
import IconStarCard from "../icon/IconStarCard";
import IconButtonClose from "../icon/IconButtonClose";

function ProductCardBasket({id, racingCar, toggleCounterSelected, toggleCounterFavorites}) {

    const [showModal, setShowModal] = useState(false);
    const {imageUrl, name, color, price, completed, } = racingCar;

    const toggleModal = () => {
        setShowModal(!showModal);
    };

    const modalActions = (
        <div className="button-container">
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="Yes" onClick={() => {toggleModal(); toggleCounterSelected(id)}}/>
            <Button backgroundColor="rgba(0, 0, 0, 0.4)" textColor="white" text="No" onClick={toggleModal}/>
        </div>
    );

    return (
        <li className="car-item-basket">
            <div className={"basket"}>
                <div className="img-container-basket">
                    <img className={"car-img-basket"} src={imageUrl} alt={name}/>
                </div>
                <div className="basket-text">
                    <h2 className="car-name">{name}</h2>
                    <p className="car-info">Цвет: {color}</p>
                    <p className="car-info">Цена: ${price.toLocaleString()}</p></div>
            </div>
            <button className={"button-basket-close"} onClick={() => toggleModal()}>
                <IconButtonClose />
            </button>
            <div onClick={() => {toggleCounterFavorites(id)}} className={`car-item-icon-basket ${completed && "completed"}`}>
                <IconStarCard/>
            </div>
            {showModal && (<Modal header="Confirmation" closeButton={true} onClick={toggleModal} text={"Are you sure you want to remove " + name + " from your cart?"} actions={modalActions}/>)}
        </li>
    );
}

ProductCardBasket.propTypes = {
    id:PropTypes.number,
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default ProductCardBasket;
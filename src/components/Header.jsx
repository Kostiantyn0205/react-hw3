import PropTypes from 'prop-types';
import { Link, useLocation } from "react-router-dom";
import cn from "classnames";
import IconStar from "../icon/IconStar";
import IconBasket from "../icon/IconBasket";
import IconHome from "../icon/IconHome";

const Header = ({counterFavorites, counterSelected}) => {
    const {pathname} = useLocation()

    return <header>
        <h1 className={"title"}>Sports Car Store</h1>
        <ul className="container-icon">
            <li>
                <Link to="/" className={cn("icon-wrapper", {active: pathname === "/"})}>
                    <IconHome />
                </Link>
            </li>
            <li>
                <Link to="/favorites" className={cn("icon-wrapper", {active: pathname === "/favorites"})}>
                    <IconStar />
                    <div className="counter counter-favorites">{counterFavorites}</div>
                </Link>
            </li>
            <li>
                <Link to="/basket" className={cn("icon-wrapper", {active: pathname === "/basket"})}>
                    <IconBasket/>
                    <div className="counter counter-selected">{counterSelected}</div>
                </Link>
            </li>
        </ul>
    </header>
}

Header.propTypes = {
    counterFavorites: PropTypes.number,
    counterSelected: PropTypes.number
};

export default Header
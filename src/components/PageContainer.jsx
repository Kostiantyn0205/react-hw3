import Header from "./Header";
import PropTypes from "prop-types";
import ProductCardBasket from "./ProductCardBasket";

function PageContainer({children, pageName, counterFavorites, counterSelected}) {
    return (
        <div>
            <Header counterFavorites={counterFavorites} counterSelected={counterSelected} pageName={pageName}/>
            {children}
        </div>
    )
}

PageContainer.propTypes = {
    pageName:PropTypes.string,
    counterFavorites: PropTypes.number,
    counterSelected: PropTypes.number
};

export default PageContainer
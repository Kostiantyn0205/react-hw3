import './scss/App.scss';
import {useEffect, useState} from 'react';
import {Routes, Route} from "react-router-dom";
import Store from "./pages/Store"
import Basket from "./pages/Basket"
import Favorites from "./pages/Favorites"

function App() {
    const localStorageDataFavorites = JSON.parse(localStorage.getItem("counterFavorites"));
    const localStorageDataSelected = JSON.parse(localStorage.getItem("counterSelected"));
    const localStorageRacingCars = JSON.parse(localStorage.getItem("racingCars"));

    const [counterFavorites, setCounterFavorites] = useState( localStorageDataFavorites || 0);
    const [counterSelected, setCounterSelected] = useState( localStorageDataSelected || 0);
    const [racingCars, setRacingCars] = useState(localStorageRacingCars || []);

    useEffect(() => {
        localStorage.setItem("counterFavorites", JSON.stringify(counterFavorites));
        localStorage.setItem("counterSelected", JSON.stringify(counterSelected));
    }, [counterSelected, counterFavorites]);

    useEffect(() => {
        if (!localStorageRacingCars) {
            fetch('/products.json')
                .then((response) => {
                    if (!response.ok) {
                        throw new Error('Error executing the request');
                    }
                    return response.json();
                })
                .then((data) => {
                    data.racingCars.forEach((data, index) => {
                        data.completed = false;
                        data.idCar = index;
                        data.buyCar = false;
                        index++;
                    });
                    setRacingCars(data.racingCars);
                })
                .catch((error) => {
                    console.error('Error executing the request:', error);
                });
        }
    }, []);

    const toggleCounterFavorites = (id) => {
        const newRacingCars = [...racingCars];
        newRacingCars[id].completed = !newRacingCars[id].completed;
        newRacingCars[id].completed? setCounterFavorites(counterFavorites+1):setCounterFavorites(counterFavorites-1);
        setRacingCars(newRacingCars);
        localStorage.setItem("racingCars", JSON.stringify(newRacingCars));
    }

    const toggleCounterSelected = (id) => {
        const newRacingCars = [...racingCars];
         newRacingCars[id].buyCar = !newRacingCars[id].buyCar;
         newRacingCars[id].buyCar? setCounterSelected(counterSelected+1):setCounterSelected(counterSelected-1);
         setRacingCars(newRacingCars);
         localStorage.setItem("racingCars", JSON.stringify(newRacingCars));
    }

    return (
        <Routes>
            <Route index element={<Store counterSelected={counterSelected} counterFavorites={counterFavorites} racingCars={racingCars}  toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites} />}/>
            <Route path="/" element={<Store counterSelected={counterSelected} counterFavorites={counterFavorites} racingCars={racingCars}  toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites} />}/>
            <Route path="/favorites" element={<Favorites counterSelected={counterSelected} counterFavorites={counterFavorites} racingCars={racingCars} toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}  />}/>}/>}/>
            <Route path="/basket" element={<Basket counterSelected={counterSelected} counterFavorites={counterFavorites} racingCars={racingCars}toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites} />}/>}/>
        </Routes>
    );
}

export default App;
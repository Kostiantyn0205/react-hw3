import PageContainer from "../components/PageContainer";
import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";
import ProductCardBasket from "../components/ProductCardBasket";

function Basket({counterSelected, counterFavorites, racingCars, toggleCounterSelected, toggleCounterFavorites}){
    const completedRacingCars = racingCars.filter(racingCar => racingCar.buyCar === true);
    return (
        <PageContainer pageName={"Basket"} counterFavorites={counterFavorites} counterSelected={counterSelected} className="App">
            <ListCard pageName="Basket" racingCars={completedRacingCars} toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}></ListCard>
        </PageContainer>
    )
}

Basket.propTypes = {
    counterFavorites: PropTypes.number,
    counterSelected: PropTypes.number,
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Basket;
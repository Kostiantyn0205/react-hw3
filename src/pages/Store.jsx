import ListCard from "../components/ListCard;";
import Modal from "../components/Modal";
import PageContainer from "../components/PageContainer";
import PropTypes from "prop-types";
import Favorites from "./Favorites";

function Store({counterSelected, counterFavorites, racingCars, toggleCounterSelected, toggleCounterFavorites}) {
    return (
        <PageContainer pageName={"Home"} counterFavorites={counterFavorites} counterSelected={counterSelected} className="App">
            <ListCard racingCars={racingCars} toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}></ListCard>
        </PageContainer>
    )
}

Store.propTypes = {
    counterFavorites: PropTypes.number,
    counterSelected: PropTypes.number,
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Store;
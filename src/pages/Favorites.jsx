import PageContainer from "../components/PageContainer";
import ListCard from "../components/ListCard;";
import PropTypes from "prop-types";
import Basket from "./Basket";

function Favorites({counterSelected, counterFavorites, racingCars, toggleCounterSelected, toggleCounterFavorites}){
    const completedRacingCars = racingCars.filter(racingCar => racingCar.completed === true);
    return (
        <PageContainer pageName={"Favorites"} counterFavorites={counterFavorites} counterSelected={counterSelected} className="App">
            <ListCard racingCars={completedRacingCars} toggleCounterSelected={toggleCounterSelected} toggleCounterFavorites={toggleCounterFavorites}></ListCard>
        </PageContainer>
    )
}

Favorites.propTypes = {
    counterFavorites: PropTypes.number,
    counterSelected: PropTypes.number,
    racingCars: PropTypes.array,
    toggleCounterSelected: PropTypes.func,
    toggleCounterFavorites: PropTypes.func
};

export default Favorites;